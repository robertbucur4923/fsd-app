import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.scss']
})
export class DisplayComponent implements OnInit {

  constructor(private http: HttpClient) { }

  result:any;
  ngOnInit(): void {
    this.get();
    console.log(this.result);
  }

   get(){
     this.http.get("https://us-central1-graphical-mile-310707.cloudfunctions.net/helloWorld").subscribe((rezultat:any) =>{
      this.result=rezultat.msg;
    })
  }


}
